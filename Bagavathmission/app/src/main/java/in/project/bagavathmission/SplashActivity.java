package in.project.bagavathmission;

import androidx.appcompat.app.AppCompatActivity;
import in.project.bagavathmission.Model.GeneralModel;
import in.project.bagavathmission.Retrofit.ApiClient;
import in.project.bagavathmission.Retrofit.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import static android.widget.Toast.LENGTH_LONG;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_splash);
        showMainScreen();
    }

    private void showMainScreen() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Launch Main Activity here
                    String data = getIntent().getStringExtra("click_action");
                    Log.d(TAG,"Intent Extra: " + data);
                        Intent i = new Intent(getBaseContext(), MainActivity.class);
                        i.putExtra("click_action_url",data);
                        startActivity(i);
                        finish();
                    }
                }, 2000);
    }
}
