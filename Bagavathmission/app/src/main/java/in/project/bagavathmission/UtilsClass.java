package in.project.bagavathmission;

import android.app.Activity;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

public class UtilsClass {


    public static void makeStatusBarTransparent(Window window, Context context)
    {
        ((Activity)context).requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }
}
