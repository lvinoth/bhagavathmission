package in.project.bagavathmission;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = MainActivity.class.getSimpleName();
    private String postUrl = "";
    private String baseUrl = "http://sribagavathmission.org";
    private WebView webView;
    private Dialog mOverlayDialog;
    public Activity baseActivity;
    String source;
    public static boolean isRunning = false;
    boolean clearHistory = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_main);
        createLoader(this);
        baseActivity = this;
        initializeFirebase();
        postUrl = baseUrl;
        isRunning = true;
        this.webView = (WebView) findViewById(R.id.webView1);
        registerReceiver(broadcastReceiver, new IntentFilter("new_article"));
        source = getIntent().getStringExtra("click_action_url");
        if (source != null && !source.equals(""))
        {
            postUrl = source;
        }
//        Toast.makeText(this,"Intent value: " + source ,Toast.LENGTH_LONG).show();
        Log.d(TAG,"Intent value: " + source);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setHorizontalScrollBarEnabled(false);


        this.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
//                progressBar.setProgress(progress);
            }
        });

        this.webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                progressBar.setVisibility(View.VISIBLE);
                if (!MainActivity.this.mOverlayDialog.isShowing()){

                    //MainActivity.this.mOverlayDialog.show();
                }

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
//                progressBar.setVisibility(View.GONE);
                MainActivity.this.mOverlayDialog.dismiss();
                if (clearHistory)
                {
                    clearHistory = false;
                    MainActivity.this.webView.clearHistory();
                }
            }
        });

        this.webView.loadUrl(postUrl);
        Log.e(MainActivity.class.getSimpleName(), "Webpage is available!");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        source = intent.getStringExtra("click_action");
        if (source != null && !source.equals(""))
        {
            webView.loadUrl(source);
        }

        //Toast.makeText(this,"New Intent Delivered!: " + source,Toast.LENGTH_LONG).show();
        Log.d(TAG,"New Intent Delivered!"+ source);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
        isRunning = false;
        if(this.mOverlayDialog!= null && this.mOverlayDialog.isShowing())
            this.mOverlayDialog.dismiss();
        Log.d(TAG,"Activity Destroyed!!!!!");
    }

    public void initializeFirebase()
    {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void createLoader(Context context)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View progressLayout = inflater.inflate(R.layout.progress, null);

        this.mOverlayDialog = new Dialog(context);
        this.mOverlayDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.mOverlayDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.mOverlayDialog.setContentView(progressLayout);
        this.mOverlayDialog.setCancelable(false);
        this.mOverlayDialog.setCanceledOnTouchOutside(false);
//        this.mOverlayDialog.show();
    }

    private void createAlert()
    {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_alert);

        TextView text = dialog.findViewById(R.id.messageView);
        text.setText("Do you want to close the app?");
        TextView yesBtn = dialog.findViewById(R.id.yesBtn);
        TextView noBtn = dialog.findViewById(R.id.noBtn);

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                baseActivity.finishAndRemoveTask();
            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            String newUrl = intent.getStringExtra("url");
            if (action.equals("new_article")) {
                MainActivity.this.webView.loadUrl(newUrl);
            }
        }
    };

    @Override
    public void onBackPressed() {
        if(this.webView.canGoBack() || (source != null && !source.equals("")))
        {
            source = "";
            this.webView.loadUrl(baseUrl);
            clearHistory = true;
        }
        else {
            createAlert();
        }
//
//        if (source != null && source.equals("notification"))
//        {
//            source = "";
//            webView.loadUrl(baseUrl);
//            clearHistory = true;
//        }
//        else {
//            if (webView.canGoBack())
//            {
//                webView.goBack();
//            }
//            else {
//                createAlert();
//            }
//        }
    }
}
