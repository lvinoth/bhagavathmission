package in.project.bagavathmission.Model;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class GeneralModel {


    private String status;

    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
