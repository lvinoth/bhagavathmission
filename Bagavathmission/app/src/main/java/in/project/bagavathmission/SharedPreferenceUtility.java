package in.project.bagavathmission;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtility {

    private static SharedPreferences sharedPreferences;


    public static SharedPreferences getSharedPreferenceInstance(Context context) {
        if (sharedPreferences == null)
            sharedPreferences = context.getSharedPreferences("user_preferences",
                    Context.MODE_PRIVATE);

        return sharedPreferences;
    }

    public static void saveActionClick(Context context, String actionClick) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();

        sharedPreferencesEditor.putString("usr_token", actionClick);
        sharedPreferencesEditor.commit();
    }

    public static void clearActionClick(Context context) {

        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();
        sharedPreferencesEditor.remove("usr_token");
        sharedPreferencesEditor.commit();
    }


    public static String getActionClick(Context context) {
        return getSharedPreferenceInstance(context).getString("usr_token", "");
    }
}
