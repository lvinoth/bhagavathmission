package in.project.bagavathmission.Retrofit;

import in.project.bagavathmission.Model.GeneralModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

//    @FormUrlEncoded
//    @POST("field_executive_registration")
//    Call<RegistrationModel> executiveRegistration(@Field("fe_name") String name, @Field("fe_mobile") String mobileNo,
//                                                  @Field("fe_latitude") String latitude, @Field("fe_longitude") String longitude);
    @FormUrlEncoded
    @POST("set_device_tokens.php")
    Call<GeneralModel> postDeviceToken(@Field("unique_id") String uniqueId, @Field("token_id") String tokenId);


}
